Ayodele Hamilton
Jan. 22, 2015
CMPSC 112

1. A description of the steps you took to install the spf13-vim distribution.
    To install the spf13-vim on the computer, the steps to follow are:
        a.From the spf13-vi website, install the msysgit.
        b.After installing that, in your terminal window run the git --
          version.
        c. Now setup your curl; by typing in the terminal window curl 
           and press answer.
        d. Then assuming that your msysgit wasgood to run then the program 
           should work with the new plugins. 

2. A full- featured description of the basic features associated with the Vim text editor. 
    Vim text editor has a way to catch syntactical errors found in the program. It has 
    shortcuts to make typing up your program a lot easier. The Vim text editor has line 
    numbers to help keep of what lines have errors. The Vim text editor makes writing a new
    program much easier to write and read. The program makes sure that it is colored coded for the 
    reserved words and to help format the program.

3. A complete introduction to the use of the aforementioned Vim plugins.
    ~Ctrl-P- It shows all the files without having to open each one in GVim
    ~Fugitive- This is a way to help commit files to BitBucket.
    ~NERDTree- It opens on the side and maps out all of your files. 
    ~NerdCommenter- It shows all the program comments.
    ~EasyMotion- It helps to jump to a certain place in the program.
    ~Tabularize- This helps format the program.
    ~Tagbar- It helps to navigate the program by organizing into different 
             parts.
