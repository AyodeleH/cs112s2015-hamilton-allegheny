//*******************************************
//HonorCode: The work I am submitting is a result of my own thinking and efforts.
//Ayodele Hamilton
//CMPSC 112 Spring 2015
//Lab # 8
//Date: 3/24/15
//
//Purpose: Performing Arithmetic Interpretation with a Stack
//********************************************

import java.util.Stack;
import java.util.Scanner;

public class StackMachine
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        Stack<String> stack = new Stack<>();
        String ch;
        String y;
        String z;
       // input = scan.nextLine();
        boolean empty = false;

        System.out.println("Welcome to the Stack Machine!!!");
        //System.out.println("Enter 1 of the 2 folLowing characters: +, s. Enter in two integers");
        //ch = scan.nextLine();
        //y = scan.nextLine();
        //z = scan.nextLine();
        System.out.println("Available options: d: Dsplay Stack, e: Evaluate, x:Quit");
        //String command =  scan.next();

        //System.out.println("Enter a +(plus sign) or an s");
       // ch = scan.nextLine();
       // stack.push(ch);

        System.out.println("Enter in a number");
        y = scan.nextLine();
        stack.push(y);

        System.out.println("Enter in another number");
        z =scan.nextLine();
        stack.push(z);

        System.out.println("Enter a +(plus sign) or an s");
        ch = scan.nextLine();
        stack.push(ch);

     String command =  scan.next();
        while(command != "x")
        {
            String commands =  scan.nextLine();

            if(commands.equals("d"))
            {
                System.out.println("Display Stack");
                System.out.println(stack);
            }

             else if(commands.equals("e"))
            {
                System.out.println("Evaluate Stack");
                System.out.println("Top of the Stack" + stack.peek());
                if(stack.peek().equals("+"))
                {
                    System.out.println("Do addition");
                    ch = stack.pop().toString();
                    y = stack.pop().toString();
                    z = stack.pop().toString();

                    int d = Integer.parseInt(y);
                    int e = Integer.parseInt(z);
                    int value = d + e;

                    String result = Integer.toString(value);

                    stack.push(result);
                }

               else if(stack.peek().equals("s"))
                {
                    y = stack.pop().toString();
                    z = stack.pop().toString();

                    stack.push(y);
                    stack.push(z);
                }
            }

            else if(commands.equals("x"))
            {
                break;
            }
        }
    }
}

