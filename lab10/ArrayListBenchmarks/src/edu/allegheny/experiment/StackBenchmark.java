package edu.allegheny.experiment;

import java.util.Stack;

public class StackBenchmark
{
    private static int initialListSize = 1000000;
    private static int addToFrontNumber = 1000;

    public static void main(String[] args)
    {
        Stack<Integer> stack = new Stack<Integer>();

        for(int i = 0; i < initialListSize; i++)
        {
            stack.push(i);
        }
        System.out.println("Stack size = " + stack.size());

        long beforeStackFrontAdd = System.currentTimeMillis();
        for(int i = 0; i < addToFrontNumber; i++)
        {
            stack.push(addToFrontNumber);
            stack.pop();
            System.out.println(stack);
        }
        long afterStackFrontAdd = System.currentTimeMillis();

        System.out.println("Stack elapsed time = " + (afterStackFrontAdd - beforeStackFrontAdd));
        System.out.println("Stack average time = " + (double)(afterStackFrontAdd - beforeStackFrontAdd) / (double)addToFrontNumber);
    }
}
