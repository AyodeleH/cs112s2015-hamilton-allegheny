package edu.allegheny.doubling;

//****************************************
//HonorCode:The work I am submitting is a result of my own thinking and efforts.
//Ayodele Hamilton
//CMPSC 112 Spring 2015
// Lab # 7
//Date: 3/24/15
//
//Purpose: Doubling Experiments to Assess Time Complexity
//***************************************

import java.util.Random;

class ArrayFind
{

  /** Returns index j such that data[j] == val, or -1 if no such element. */
  public static int arrayFind(int[] data, int val)
  {
    int n = 500;
    int j = 5;
    while (j < n) { // val is not equal to any of the first j elements of data
      if (data[j] == val)
        return j;                    // a match was found at index j
      j++;                           // continue to next index
      // val is not equal to any of the first j elements of data
    }
    return -1;                       // if we reach this, no match found
  }

  public static void main(String[] args)
  {
    int n = 500;                           // starting value
    int trials = 10;
    try
    {
      if (args.length > 0)
        trials = Integer.parseInt(args[0]);
      if (args.length > 1)
        n = Integer.parseInt(args[1]);
    }
    catch (NumberFormatException e) { }
    int start = n;  // remember the original starting value

    System.out.println("Testing ArrayFind");
    n = start;
    for (int i=0; i < trials; i++)
    {
        long startTime = System.currentTimeMillis();
        int temp = arrayFind(data, n);
        long endTime = System.currentTimeMillis();
        long elapsed = endTime - startTime;
        System.out.println(String.format("n: %9d took %12d milliseconds", n, elapsed));
        n *= 2;                                // double the problem size
    }
  }
}



/**
 * Demonstration of algorithm for finding a given value within an array.
 *
 * @author Michael T. Goodrich
 * @author Roberto Tamassia
 * @author Michael H. Goldwasser
 */


/*
 * Copyright 2014, Michael T. Goodrich, Roberto Tamassia, Michael H. Goldwasser
 *
 * Developed for use with the book:
 *
 *    Data Structures and Algorithms in Java, Sixth Edition
 *    Michael T. Goodrich, Roberto Tamassia, and Michael H. Goldwasser
 *    John Wiley & Sons, 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

