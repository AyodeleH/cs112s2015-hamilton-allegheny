Ayodele Hamilton
1/15/15

1. Description of the Steps to Config Git and Bitbucket
The steps the user must use in order to config Git and Bitbucket is first creating a Bitbucket account, then to create a ssh key to start. Then after the user starts that, add the key in Bitbucket. The key will appear and that will be your Bitbucket repository password. Create the repository and then connect it to your Bitbucket to make sure that they are linked to your local computer from the network.

2. Git Commands
“git init” is to intialized the beginning stage of the repository.
“git status” is to check where the file is being added to or to find out where it is at that point.
“git add” is a way for Bitbucket to know which file to be added.
“git commit” is the command that will make sure that the file will be ttansferrign to the Bitbucket file. “git push” pushes the file to Bitbucket to be viewed.
“git pull” takes the files that are already in Bitbucket and pulls it toward your repository in order to share files.






Ayodele Hamilton
1/15/15
3. Commented version of the two Jva files
Weeee.java
public class Weeee {

    public static void weeee() {
        System.out.println("Weeee!");
        weeee();
    }

    public static void main(String[] args) {
        weeee();
    }
//Since the Weeee.java file doesn't have a while loop, it crashes.
//The code runs for about 2 seconds and then crashes due to there not
//being a while, for, or do-while to be infinite.
}


Hooray.java

public class Hooray {

    public static void hooray() {
        while(true) {
            System.out.println("Hooray!");
        }
    }

    public static void main(String[] args) {
        hooray();
    }
//The file Hooray.java is an infinite loop where there is no end to the
//code. The code uses the while loop for the infinite loop.
}









Ayodele Hamilton
1/15/15
4.
	a. When compiling and running the programs, first type in javac for the compiler to run. Once 	there aren't any problems with your program, type java and the name of your file, then it will 	run.

	b. The output that the Hooray.java gave was the inifinte loop of Hooray, while the Weeee.java 	only ran for a while and stopped because there wasn't a while, do-while, or for to say that it 	should continue for this amount of time.

	c. These programs ran like this because the Hooray.java had the while loop but the Weeee.java 	didn't have any loop to keep it going continuously.

	d. The similarities between the two programs is that they look the same and their functions are 	sort of similar. The difference is that the Hooray.java runs infintely while the Weeee.java is 	finite. The Weeee.java doesnt have a loop to keep it going.

